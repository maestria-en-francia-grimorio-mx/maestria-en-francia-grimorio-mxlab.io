---
title: Primeros Pasos
date: 2022-02-26
tags:
  - Master
---

Lo primero es aceptar que este proceso lleva mucho tiempo de planeación y anticipación. Como dicen por ahí: *Con esfuerzo y perseverancia podrás alcanzar tus metas*.

Todo lo que está aquí no aseguran la obtención de una beca, pero aumentan las posibilidades de ir a estudiar a Francia.

Los principales pasos a realizar son los siguientes:  

- [Paso X. Investigar convocatorias](https://link)
- [Paso X. Escoger maestría](https://link)
- [Paso X. Crear proyecto profesional maestría](https://link)
- [Paso X. Traducción de documentos](https://link)
- [Paso X. Crear cuenta en Espacio Campus France](https://link)

