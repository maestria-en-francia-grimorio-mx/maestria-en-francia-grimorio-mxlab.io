---
title: Requerimentos para una beca
type: posts
date: 2022-02-26
tags:
  - Master
---

{{< hint info >}}
Los requisitos varían de una convocatoría a otra, sin embargo después de leer bastantes convocatorias se puede resumir a los siguientes.
{{< /hint >}}



## Requisitos obligatorios

- Título de licenciatura o ingeniería en México (Original y traducido al francés).
- Obtener un promedio mayor a 8.5 (Depende de la convocatoría pero generalmente es el promedio solicitado).
- Certificación mínima DELF B2 (en algunos casos piden C1).
- Kardex o boleta de calificaciones de toda la carrera (Original y traducido al francés).
- Currículum (Original, traducido al francés y en algunas ocasiones traducido al inglés).
- Carta de motivos (en francés o inglés).
- Carta de aceptación de la universidad a estudiar.
- Plan de estudios de la maestría a postular.
- Acta de nacimiento (original, copia, traducciones y apostillada).
- Pasaporte vigente (mínimo el periodo que vas a pasar en la maestría).

## Requisitos extra

- Tener menos de 30 años.
- Obtener un puntaje mínimo en el Test de Connaissance du Français (TCF).
- Experiencia laboral.
- Proyecto profesional.



