---
title: Lista de tareas pendientes
date: 2022-04-01
tags:
  - misc
---

Aquí se muestran las tareas que faltan por realizar en esta página:

## Información

### Estudiar maestría en Francia

- [ ] Paso X. Investigar convocatorias
- [ ] Paso X. Escoger maestría
- [ ] Paso X. Crear proyecto profesional maestría
- [ ] Paso X. Traducción de documentos
- [ ] Paso X. Crear cuenta en Espacio Campus France


## Estética

- [ ] Arreglar el favicon.
- [ ] Agregar imágenes a los sitios para hacerlos más bonitos.

## Traducciones

- [ ] Traducir las páginas a Francés.

## Templates

- [ ] Agregar template para CV.
- [ ] Agregar template para lettre de motivation.
- [ ] Agregar template para proyecto profesional.
