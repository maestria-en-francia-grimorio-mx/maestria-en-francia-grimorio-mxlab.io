---
title: Herramientas
date: 2022-04-01
tags:
  - misc
---

Esta sección contiene herramientas que puedes llegar a necesitar durante el proceso:

| Sitio Web        | URL                              | Descripción                                                                                                           |
|------------------|----------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| TinyWow          | https://tinywow.com/             | Contiene herramientas para PDF, imágenes y videos, por ejemplo, fusionar 2 o más archivos PDF en un solo archivo PDF. |
| DeepL Translator | https://www.deepl.com/translator | Traductor de diferentes idiomas bastante potente. Las traducciones son mejores que Google Translate.                  |
| Z-Library        | https://z-lib.org/               | Biblioteca virtual donde se encuentran prácticamente cualquier libro.                                                 |

