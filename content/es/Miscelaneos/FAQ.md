---
title: Preguntas frecuentes
date: 2022-02-26
tags:
  - FAQ
---

{{< expand "¿Necesitas una visa para estudiar en Francia?" "..." >}}

  Los estudiantes intencionales de los países de la Union Europea (UE) y del Espacio Económico Europeo (EEE) no necesitan una visa para estudiar en Francia. Pero aquellos fuera de estas dos zonas necesitan una visa para estudiar en francia.

  Deben solicitar una visa de estadía prolongada con un permiso de residencia, conocido como VLS-TS. Pueden obtener esto a través del proceso CEF de Campus France

{{< /expand >}}

{{< expand "¿Solo existen programas de maestría en francés?" "..." >}}

  La mayoría de los cursos se imparten en francés, sin embargo, existen varios cursos en inglés. Los estudiantes que deseen inscribirse en cursos de inglés también deben mostrar evidencia de dominio del inglés. Esto puede ser a través de la prueba de un título en un curso de inglés o mediante exámenes del idioma inglés como IELTS o TOEFL.

  {{< hint info >}}
  **Recomendación**

  Aunque existen programas en inglés, es preferible los programas en francés, ya que toda la administración y la vida en general gira en torno al idioma francés.
  {{< /hint >}}

{{< /expand >}}


{{< expand "¿Se puede trabajar mientras se estudía la maestría?" "..." >}}

  Si,sin embargo, hay ciertas limitaciones. Los estudiantes internacionales fuera del EEE pueden trabajar a tiempo parcial hasta un 60% del año laboral normal de los trabajadores franceses. Esto es parte de los permisos otorgados en la visa VLS-TS y el permiso de residencia.

  {{< hint info >}}
  Los estudiantes no son considerados como trabajadores.
  {{< /hint >}}

{{< /expand >}}

{{< expand "¿Es posible vivir y trabajar en Francia después de graduarse de la maestría?" "..." >}}

  La respuesta corta es sí. Sin embargo, es necesario solicitar un cambio de estado de residencia a través de la prefectura local. Investigar Titre de séjour salarié o autorisation provisoire de séjour (APS).

{{< /expand >}}
