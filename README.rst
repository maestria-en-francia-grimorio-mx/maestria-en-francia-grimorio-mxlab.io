.. image:: https://gitlab.com/maestria-en-francia-grimorio-mx/maestria-en-francia-grimorio-mx.gitlab.io/badges/develop/pipeline.svg
    :target: https://gitlab.com/maestria-en-francia-grimorio-mx/maestria-en-francia-grimorio-mx.gitlab.io
    :alt: Build Status

.. image:: https://img.shields.io/badge/hugo-0.83-blue.svg
    :target: https://gohugo.io
    :alt: Hugo Version

.. image:: https://img.shields.io/badge/License-GPL_v2-blue.svg
    :target: https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
    :alt: License: GNU GPLv2

===============================
 Maestría en Francia, Grimorio
===============================

.. image:: static/img/grimmoire128x128.png

Grimorio
--------

El grimorio es un libro de magia que contiene hechizos, encantamientos y rituales. Es una herramienta muy útil para aquellos que desean estudiar y practicar la magia en el extranjero. 

Con este grimorio, podrás aprender y desarrollar tus habilidades mágicas, y también descubrir nuevas formas de utilizar la magia para mejorar tu vida y la de los demás. Este grimorio incluye información detallada sobre diferentes tipos de magia, así como instrucciones paso a paso para llevar a cabo cada hechizo y ritual. Con la ayuda de este libro, podrás convertirte en un experto en magia y usarla para ayudarte a cumplir tus metas y deseos en el extranjero.

Proceso de construcción y lanzamiento
--------------------------------------

La página es creada automáticamente con el uso del CI (Continuous Integration) de Gitlab cuando la branch de master sea afectada. El script utiliza hugo para implementar el `sitio web <https://maestria-en-francia-grimorio-mx.gitlab.io/>`_ .

Si se requiere recrear la página en local, es necesario `instalar hugo <https://gohugo.io/getting-started/installing/>`_. Una vez instalado hay que ejecutar el comando siguiente:

.. code-block:: bash

    hugo server

¿ Cómo contribuir ?
---------------------

Si gustas aportar a este repositorio, por favor lee las `Instrucciones para aportar al repositorio <https://gitlab.com/maestria-en-francia-grimorio-mx/maestria-en-francia-grimorio-mx.gitlab.io/-/blob/master/CONTRIBUTING.md>`_.

Disclaimer
----------

Este sitio fue creado con `Hugo <https://gohugo.io/>`_ y Gitlab Pages. 

Las páginas estáticas de este proyecto son construidas por GitLab CI, siguiendo los pasos definidos en `.gitlab-ci.yml <./.gitlab-ci.yml>`_ .

Colaboradores
-------------

- Ever ATILANO (Creador del proyecto)
- Martin ACOSTA (Colaborador)


Un agradecimiento especial a todos los que han contribuido a este proyecto. Les agradecemos de corazón a todos.


Licencia
--------

Este proyecto tiene una licencia GNU GPLv2, para más información por favor lea el archivo `LICENSE <https://gitlab.com/maestria-en-francia-grimorio-mx/maestria-en-francia-grimorio-mx.gitlab.io/-/blob/master/LICENSE>`_  para más detalles.